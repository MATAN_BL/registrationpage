var ipaddr = "http://127.0.0.1:3000";


$('document').ready(function() {
   $('body').hide();
   $('query').empty();
   var retVal = prompt("Please type your password");
   sendPassword(retVal);
});


$('#searchBtn').click(function() {
   searchInDB($("#searchQuery").val());
});

$('#mailBtn').click(function() {
   changeMail($("#newMail").val());
});

function addTable() {
   $("#queryResults").append("<table> <tr> <th>E-mail</th> <th>Company</th>" +
   "<th> Subject</th> <th> Text </th> </tr></table>");
}

function sendPassword(mypass) {
    var xhttp = new XMLHttpRequest();
    xhttp.open("GET", ipaddr + "/pass", true);
    xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            if (this.responseText == mypass) {
               $('body').show();
               getEmail();
            } else {
                alert("Wrong Password");
            }
        }
        if (xhttp.readyState == 4 && xhttp.status > 300) {
          console.error(xhttp.responseText);
          alert("Error! Please try later");
       }
    };
    xhttp.send();
}

function changeMail(newMail) {
   console.log(newMail);
   var params = "email=" + newMail;
   var xhttp = new XMLHttpRequest();
   xhttp.open("POST", ipaddr + "/changemail?" + params, true);
   xhttp.onreadystatechange = function() {
      console.log(xhttp.readyState + " " + xhttp.status);
       if (xhttp.readyState == 4 && xhttp.status == 200) {
          console.log("request " + params + " was sent to DB");
          alert("Mail Updated!");
       }
       if (xhttp.readyState == 4 && xhttp.status > 300) {
          console.error(xhttp.responseText);
          alert("Error! Please try later");
       }
   };
   xhttp.send();
}

function addToTable(data) {
   var newRow = "";
   data.forEach(function(row) {
      newRow = newRow + "<tr> <td>" + row.email + "</td> <td>" + row.company +
         "</td><td>" + row.subject + "</td><td>" + row.text + "</td> </tr>";
   });
   $('#queryResults').prepend("<h3>" + data.length + " results were found: </h3>");
   if (data.length > 0) {
      addTable();
      $('table').append(newRow);
   }
}

function getEmail() {
   var xhttp = new XMLHttpRequest();
   xhttp.open("GET", ipaddr + "/getemail", true);
   xhttp.onreadystatechange = function() {
      console.log(xhttp.readyState + " " + xhttp.status);
      if (xhttp.readyState == 4 && xhttp.status == 200) {
         console.log(xhttp.responseText);
         $('#newMail').val(xhttp.responseText);
      }
      if (xhttp.readyState == 4 && xhttp.status > 300) {
          console.error(xhttp.responseText);
          alert("Error! Please try later");
       }
   };
   xhttp.send();
}

function searchInDB(query) {
   console.log(query);
   $('#queryResults').empty();
   var params = "query=" + query;
   var xhttp = new XMLHttpRequest();
   xhttp.open("GET", ipaddr + "/search?" + params, true);
   xhttp.onreadystatechange = function() {
      console.log(xhttp.readyState + " " + xhttp.status);
      if (xhttp.readyState == 4 && xhttp.status == 200) {
         console.log(xhttp.responseText);
         addToTable(JSON.parse(xhttp.responseText));
      }
      if (xhttp.readyState == 4 && xhttp.status > 300) {
          console.error(xhttp.responseText);
          alert("Error! Please try later");
       }
   };
   xhttp.send();
}