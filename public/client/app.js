var ipaddr = "http://127.0.0.1:3000";

console.log(ipaddr);

$("#form").submit(function() {
   addFormToDB($("#email").val(), $("#company").val(), $("#subject").val(), $("#text").val());
   return false;
});

function clearFields() {
   $("#email").val('');
   $('#company').val('');
   $('#subject').val('');
   $('#text').val('');
}

function addFormToDB(email, company, subject, text) {
   if (email == "" && company == "" && subject == "" && text == "") {
      alert("Invalid Entries");
      return;
   }
    var params = "email=" + email + "&company=" + company + "&subject=" + subject + "&text=" + text;
    var xhttp = new XMLHttpRequest();
    xhttp.open("POST", ipaddr + "/submit?" + params, true);
    xhttp.onreadystatechange = function() {
       console.log(xhttp.readyState + " " + xhttp.status);
       if (xhttp.readyState == 4 && xhttp.status == 200) {
          console.log("request " + params + " was sent to DB");
          clearFields();
          alert("Thank You!");
       }
       if (xhttp.readyState == 4 && xhttp.status > 300) {
          console.error(xhttp.responseText);
          alert("Error! Please try later");
       }
    };
    xhttp.send();
}
