var sqlite3 = require('sqlite3').verbose();
var db = new sqlite3.Database('mydb.db');
var path = require('path');
var express = require('express');
var app = express();

db.serialize(function() {
    db.run("CREATE TABLE if not exists users (email [TEXT], company[TEXT], subject[TEXT], text[TEXT])");
    db.run("CREATE TABLE if not exists email (email [TEXT])");
    db.run("CREATE TABLE if not exists password (Field1 [TEXT])");
});

app.use(express.static(path.join(__dirname, '../public/')) );

app.use(function(req, res, next){
   res.header("Access-Control-Allow-Origin", "*");
   res.header("Access-Control-Allow-Headers", "*");
   next();
});

app.get('/admin/', function(req, res) {
    res.sendFile(path.join(__dirname,'../public/adminDir/index2.html'));
});

app.get('/', function(req,res) {
  res.sendFile(path.join(__dirname,'../public/client/index.html'));
});

app.post('/submit', function(req, res) {
   console.log("here");
    var p = new Promise(function(resolve, reject) {
        db.serialize(function() {
            db.run("INSERT INTO users VALUES (?, ?, ?, ?)",
            [req.query['email'], req.query['company'], req.query['subject'], req.query['text']],
            function (err) {
                if (err) {
                    console.error(err);
                    reject();
                } else {
                    console.log("Transaction passed");
                    resolve();
                }
            });
        });
    });
    p.then(function(){
        res.status(200).send();
    }).catch(function() {
        res.status(400).send();
    })
});

app.post('/changemail', function(req, res) {
   var p = new Promise(function(resolve, reject) {
        db.serialize(function() {
            db.run("UPDATE email SET email = ?", req.query['email'],
            function (err) {
                if (err) {
                    console.error(err);
                    reject();
                } else {
                    console.log("Mail updated");
                    resolve();
                }
            });
        });
    });
    p.then(function(){
        res.status(200).send();
    }).catch(function() {
        res.status(400).send();
    })
});

app.get('/getemail', function(req, res) {
    db.serialize(function() {
      var sql = "SELECT email FROM email";
      var email = "";
      db.each(sql, function(err, row) {
         if (err) {
            console.error(err);
            res.status(400).send();
         } else {
            email = row;
            res.status(200).send(email['email'])
         }
      })
    })
});

app.get('/pass', function(req, res) {
    console.log("sending password");
    db.serialize(function() {
        db.each("SELECT * from password", function(err, row) {
            console.log("password is: " + row.Field1);
            res.send(row.Field1);
        });
  });
});

app.get('/search', function(req, res) {
   var query = req.query['query'].replace(/"/g, function() { return "" });
   db.serialize(function() {
      var sql = "SELECT * FROM users WHERE (email LIKE %s) OR (company LIKE %s) OR" +
         "(subject LIKE %s) OR (text LIKE %s)";
      sql = sql.replace(/%s/g, function() { return "'%" + query + "%'"});
      var results = [];
      db.each(sql, function(err, row) {
         if (err) {
            console.error(err);
            res.status(400).send();
         } else {
            results.push(row);
         }
      }, function () {
         console.log(results);
         res.status(200).send(results);
      });
   });
});

app.listen(process.env.PORT, function() {
  console.log("listening on port " + process.env.PORT);
});


